��          T      �       �   T   �        
     "   "     E     [  �  p  p        |  
   �  +   �     �     �                                        Adds VAT number field to WC checkout and adjusts tax rates for businesses in the EU. Turpoint VAT number VAT number could not be validated. WP WooCommerce EU VAT https://turpoint.com Project-Id-Version: WP WooCommerce EU VAT 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/turpoint-wp-wc-eu-vat
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-12-04 09:53+0100
X-Generator: Poedit 2.4.2
X-Domain: turpoint-wp-wc-eu-vat
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl_BE
 Voegt een veld voor een BTW-nummer toe aan de checkout en past de belastingtarieven aan voor bedrijven in de EU. Turpoint BTW nummer Het BTW-nummer kon niet gevalideerd worden. WP WooCommerce EU BTW https://turpoint.com 