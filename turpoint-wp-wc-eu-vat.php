<?php

/**
 * Plugin Name: WP WooCommerce EU VAT
 * Author: Turpoint
 * Author URI: https://turpoint.com
 * Description: Adds VAT number field to WC checkout and adjusts tax rates for businesses in the EU.
 * Version: 1.0.2
 * Requires at least: 5.5
 * Text Domain: turpoint-wp-wc-eu-vat
 * Domain Path: /languages
 */

require_once(dirname(__FILE__) . '/inc/class-wp-wc-eu-vat.php');

add_action('init', function() {
    load_plugin_textdomain('turpoint-wp-wc-eu-vat', false, dirname(plugin_basename( __FILE__ )) . '/languages'); 
});