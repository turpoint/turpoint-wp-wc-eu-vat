<?php

namespace WP_WC_EU_VAT\Inc;

class WP_WC_EU_VAT
{
    /**
     * Constructor
     */
    public function __construct()
    {
        // Hooks
        add_action('woocommerce_checkout_update_order_review', [$this, 'apply_intra_community_vat_regulations']);
        add_action('woocommerce_checkout_process', [$this, 'validate_vat_number']);

        // Filters
        add_filter('woocommerce_checkout_fields', [$this, 'add_checkout_fields']);
        add_filter('woocommerce_admin_billing_fields', [$this, 'add_vat_number_to_admin_billing_fields']);
    }

    /**
     * Add checkout fields
     */
    public function add_checkout_fields($fields)
    {
        $fields['billing']['billing_vat_number'] = [
            'type' => 'text',
            'required' => false,
            'label' => ucfirst(__('VAT number', 'turpoint-wp-wc-eu-vat')),
            'class' => array ('vat-number', 'update_totals_on_change' ),
            'priority' => 30
        ];
    
        return $fields;    
    }

    /**
     * Apply intra community VAT regulations
     */
    public function apply_intra_community_vat_regulations($post_data)
    {
        $countries = new \Ibericode\Vat\Countries();

        WC()->customer->set_is_vat_exempt(false);

        parse_str($post_data, $data);

        // Bail if vat number is empty
        if (!isset($data['billing_vat_number']) || empty($data['billing_vat_number'])) {
            return;
        }

        // Bail if billing country is empty
        if (!isset($data['billing_country']) || empty($data['billing_country'])) {
            return;
        }

        // Bail if the customer is billing from the same country as the store
        if (WC()->countries->get_base_country() == $data['billing_country']) {
            return;
        }

        // Make tax exempt if the customer is located in the EU
        if ($countries->isCountryCodeInEU($data['billing_country'])) {
            WC()->customer->set_is_vat_exempt(true);
        }
    }

    /**
     * Prep VAT number
     */
    public function prepare_vat_number($number)
    {
        $number = str_replace(' ', '', $number);
        $number = str_replace('.', '', $number);
        return $number;
    }

    /**
     * Validate VAT number
     */
    public function validate_vat_number()
    {
        $vat_number = sanitize_text_field($_POST['billing_vat_number'] ?? null);

        // Bail if the VAT number is empty
        if (empty($vat_number)) {
            return;
        }

        // Bail if the VAT number isn't validated
        $vat_number = $this->prepare_vat_number($vat_number);
        $validator = new \Ibericode\Vat\Validator();
        if (!$validator->validateVatNumber($vat_number)) {
            wc_add_notice(__('VAT number could not be validated.', 'turpoint-wp-wc-eu-vat'), 'error');
        }
    }

    /**
     * Add invoice data to billing address
     */
    public function add_vat_number_to_admin_billing_fields($billing_fields)
    {
        $billing_fields['vat_number'] = [
            'label' => __('VAT number', 'turpoint-wp-wc-eu-vat')
        ];
    
        return $billing_fields;
    }
}

/**
 * Callback
 */
$wp_wc_eu_vat = new WP_WC_EU_VAT();